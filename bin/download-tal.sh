files="https://rpki.afrinic.net/tal/afrinic.tal \
        https://tal.apnic.net/apnic.tal \
        https://tal.rpki.ripe.net/ripe-ncc.tal \
        https://www.arin.net/resources/manage/rpki/arin.tal \
        https://www.lacnic.net/innovaportal/file/4983/1/lacnic.tal "

for file in $files
do
  curl -o $(basename $file) $file
done
